# To change this template, choose Tools | Templates
# and open the template in the editor.
__author__="savruk"
__date__ ="$Jun 7, 2010 6:48:12 PM$"

import urllib2
import os
import settings
import oauth2
from oauthtwitter import OAuthApi
import pprint
import feedparser

class functions:
    #def __init__(self):
    
    def readRss(self, url):
        return feedparser.parse(url);
    
    def getTwitterImageFrom(self, url):
        picture_page = url
        opener1 = urllib2.build_opener()
        page1 = opener1.open(picture_page)
        my_picture = page1.read()
        filename = "my_image" + picture_page[-4:]
        fout = open("./images/twitter/"+filename, "wb")
        fout.write(my_picture)
        fout.close()
        return "./images/twitter/"+filename
    
    def getuserTwitterImageFrom(self,url,username):
        picture_page = url
        opener1 = urllib2.build_opener()
        page1 = opener1.open(picture_page)
        my_picture = page1.read()
        filename = username + picture_page[-4:]
        fout = open("./images/twitter/"+filename, "wb")
        fout.write(my_picture)
        fout.close()
        return "./images/twitter/"+filename
    
    def userTwitterImageExits(self,url,username):
        picture_page = url
        filename = username + picture_page[-4:]
        if os.path.isfile("./images/twitter/"+filename):
            return True
        else:
            return False
    
class tweets:

    def __init__(self):
        consumer_key = "3vuUF3E4wYVzydFgHbzXaw"
        consumer_secret = "9OvGGhj72mk8zVoBrl213Ons54ecYSxTEcSf5xdnc"
        accSettings=settings.settingsXml()
        self.pp = pprint.PrettyPrinter(indent=4)
        self.twitOauthToken=accSettings.getTwitterUserOauthToken()
        self.twitpOauthTokenSecret=accSettings.getTwitterUserOauthTokenSecret()
        
        self.api=OAuthApi(consumer_key, consumer_secret, self.twitOauthToken, self.twitpOauthTokenSecret)
        
    def getUser(self):
        #user = self.api.GetUserTimeline()
        user = self.api.UserShow()
        pp = pprint.PrettyPrinter(indent=4)
        #pp.pprint(user)
        return user
    
    def GetFollowerCount(self):
        follower = self.api.GetFollowerIds()    
        return str(len(follower))
    
    def GetFriendsCount(self):
        following = self.api.GetFriendIds()
        return str(len(following))
        
    
    '''gets user's friends tweets'''
    def getUsersFriendsTweet(self):
        stats=self.api.GetFriendsTimeline()
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(stats)
        return stats
