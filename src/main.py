from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtNetwork import *
from ui_mainwindow import Ui_MainWindow
from ui_login import Ui_LoginWindow
import sys
import settings

class Main(QMainWindow):
    def __init__(self, parent=None):
        self.settings=settings.settingsXml()
        accinf=self.settings.checkAccounts()
        if accinf:
            QMainWindow.__init__(self, parent)
            mainWin=Ui_MainWindow()
            mainWin.setupUi(self)
        else:
            QMainWindow.__init__(self, parent)
            self.uilogin=Ui_LoginWindow()
            self.uilogin.setupUi(self)
            self.accType=""
            self.connect(self.uilogin.okButton, SIGNAL("clicked()"),self.okButtonClicked)
            self.connect(self.uilogin.ffButton, SIGNAL("clicked()"),self.onffButtonClicked)
            self.connect(self.uilogin.twitButton, SIGNAL("clicked()"),self.ontwittButtonClicked)
        

    def ontwittButtonClicked(self):
        self.accType="Twitter"
        if self.uilogin.ffButton.isChecked():
            self.uilogin.ffButton.setChecked(False)
        
        if self.uilogin.gridLayoutWidget.isVisible() is False:
            self.uilogin.gridLayoutWidget.setVisible(True)
            
        self.uilogin.accLabel.setText("Twitter Account")
        self.uilogin.errorLabel.setText("")
        
        
    def onffButtonClicked(self):
        self.accType="FriendFeed"
        if self.uilogin.twitButton.isChecked():
            self.uilogin.twitButton.setChecked(False)
            
        if self.uilogin.gridLayoutWidget.isVisible() is False:
            self.uilogin.gridLayoutWidget.setVisible(True)
            
        self.uilogin.errorLabel.setText("")
        self.uilogin.accLabel.setText("FriendFeed Account")
        
        
    def okButtonClicked(self):
        if not self.uilogin.twitButton.isChecked() and not self.uilogin.ffButton.isChecked():
            self.uilogin.errorLabel.setText("Select an account type")
        elif self.uilogin.usernameLine.text() == "" or self.uilogin.passwordLine.text() == "":
            self.uilogin.errorLabel.setText("Username|Password cannot be empty!")
        else:
            self.settings.createXml(self.accType,self.uilogin.usernameLine.text(), self.uilogin.passwordLine.text())
if __name__ == "__main__":

    app = QApplication(sys.argv)
    window = Main()
    window.show()
    sys.exit(app.exec_())
