from xml.dom import minidom

class settingsXml:

    def __init__(self):
        self.dom=minidom.parse('./settings.xml')
    def createXml(self,acc,username,password):
        '''asdasd'''
        file=open('./settings.xml','w')
        xmlStr="\
<?xml version='1.0'?>\n\
<settings>\n\
    <acc-settings>\n\
        <acc>\n\
            <acc-type>"+acc+"</acc-type>\n\
            <oauth_token>"+username+"</oauth_token>\n\
            <oauth_token_secret>"+password+"</oauth_token_secret>\n\
        </acc>\n\
    </acc-settings>\n\
</settings>"
        file.write(xmlStr)
        
    def checkAccounts(self):
        '''Daha once tanimlanan hesap varmi bakacak'''
        
        acctype=self.dom.getElementsByTagName('acc-type')        
        status=False
        for acc in range(len(acctype)):
            if self.getText(self.dom.getElementsByTagName('acc-type')[acc].childNodes) =="":
                status=False
            else:
                status=True
                break
        return status
    
    '''Gets user Twitter username'''
    def getTwitterUserOauthToken(self):
        accInf=self.getAccountsInf()
        username=""
        for acc in range(len(accInf)):
            if accInf[acc] == "Twitter":
                username=self.getText(self.dom.getElementsByTagName('oauth_token')[acc].childNodes)
        return username
    
    '''Gets user Twitter password'''
    def getTwitterUserOauthTokenSecret(self):
        accInf=self.getAccountsInf()
        password=""
        for acc in range(len(accInf)):
            if accInf[acc] == "Twitter":
                password=self.getText(self.dom.getElementsByTagName('oauth_token_secret')[acc].childNodes)
        return password
        
    def getText(self,nodelist):
        rc = []
        for node in nodelist:
            if node.nodeType == node.TEXT_NODE:
                rc.append(node.data)
        return ''.join(rc)   
    
    def getAccountsInf(self):
        acctype=self.dom.getElementsByTagName('acc-type')
        accs=[]
        for acc in range(len(acctype)):
            if self.getText(self.dom.getElementsByTagName('acc-type')[acc].childNodes) != "":
                accs.append(self.getText(self.dom.getElementsByTagName('acc-type')[acc].childNodes))
        return accs
    
    def getRssInfo(self):
        rsss=self.dom.getElementsByTagName('rss')    
        rssAcs=[]
        for rss in range(len(rsss)):
            rssAcs.append(self.getText(self.dom.getElementsByTagName('name')[rss].childNodes))
        return rssAcs
    def getRssLinks(self):
        rsss=self.dom.getElementsByTagName('rss')    
        links=[]
        for rss in range(len(rsss)):
            links.append(self.getText(self.dom.getElementsByTagName('link')[rss].childNodes))
        return links