# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'login.ui'
#
# Created: Tue Jun 29 01:34:12 2010
#      by: PyQt4 UI code generator 4.7.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

class Ui_LoginWindow(object):
    def setupUi(self, LoginWindow):
        LoginWindow.setObjectName("LoginWindow")
        LoginWindow.resize(501, 245)
        LoginWindow.setStyleSheet("background-color: #333333;\n"
"font: 10pt \"Lucida Sans Typewriter\";\n"
"color:#BDBDBD;")
        self.centralwidget = QtGui.QWidget(LoginWindow)
        self.centralwidget.setStyleSheet("None")
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayoutWidget = QtGui.QWidget(self.centralwidget)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(190, 40, 271, 117))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.passwordLine = QtGui.QLineEdit(self.gridLayoutWidget)
        self.passwordLine.setStyleSheet("color: #666666;")
        self.passwordLine.setObjectName("passwordLine")
        self.gridLayout.addWidget(self.passwordLine, 3, 1, 1, 1)
        self.usernameLine = QtGui.QLineEdit(self.gridLayoutWidget)
        self.usernameLine.setStyleSheet("color: #666666;\n"
"font: 10pt \"Lucida Sans Typewriter\";")
        self.usernameLine.setObjectName("usernameLine")
        self.gridLayout.addWidget(self.usernameLine, 1, 1, 1, 1)
        self.usernameLabel = QtGui.QLabel(self.gridLayoutWidget)
        self.usernameLabel.setObjectName("usernameLabel")
        self.gridLayout.addWidget(self.usernameLabel, 1, 0, 1, 1)
        self.passLabel = QtGui.QLabel(self.gridLayoutWidget)
        self.passLabel.setObjectName("passLabel")
        self.gridLayout.addWidget(self.passLabel, 3, 0, 1, 1)
        self.okButton = QtGui.QPushButton(self.gridLayoutWidget)
        self.okButton.setObjectName("okButton")
        self.gridLayout.addWidget(self.okButton, 4, 1, 1, 1)
        self.accLabel = QtGui.QLabel(self.gridLayoutWidget)
        self.accLabel.setText("")
        self.accLabel.setObjectName("accLabel")
        self.gridLayout.addWidget(self.accLabel, 0, 0, 1, 2)
        self.twitButton = QtGui.QPushButton(self.centralwidget)
        self.twitButton.setGeometry(QtCore.QRect(10, 50, 160, 40))
        self.twitButton.setText("")
        self.twitButton.setIconSize(QtCore.QSize(155, 36))
        self.twitButton.setCheckable(True)
        self.twitButton.setFlat(False)
        self.twitButton.setObjectName("twitButton")
        self.ffButton = QtGui.QPushButton(self.centralwidget)
        self.ffButton.setGeometry(QtCore.QRect(10, 110, 160, 40))
        self.ffButton.setText("")
        self.ffButton.setIconSize(QtCore.QSize(155, 36))
        self.ffButton.setCheckable(True)
        self.ffButton.setFlat(False)
        self.ffButton.setObjectName("ffButton")
        self.errorLabel = QtGui.QLabel(self.centralwidget)
        self.errorLabel.setGeometry(QtCore.QRect(140, 190, 321, 17))
        self.errorLabel.setStyleSheet("color: rgb(255, 0, 0);")
        self.errorLabel.setText("")
        self.errorLabel.setObjectName("errorLabel")
        LoginWindow.setCentralWidget(self.centralwidget)
        fflogo="../ui/images/ff_logo.png"
        twitlogo="../ui/images/twitter_logo.png"
        self.ffButton.setIcon(QtGui.QIcon(fflogo))
        self.twitButton.setIcon(QtGui.QIcon(twitlogo))
        self.gridLayoutWidget.setVisible(False)
        self.retranslateUi(LoginWindow)
        QtCore.QObject.connect(self.okButton, QtCore.SIGNAL("clicked()"), self.okButton.toggle)
        QtCore.QMetaObject.connectSlotsByName(LoginWindow)

    def retranslateUi(self, LoginWindow):
        LoginWindow.setWindowTitle(QtGui.QApplication.translate("LoginWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        self.usernameLabel.setText(QtGui.QApplication.translate("LoginWindow", "User Name", None, QtGui.QApplication.UnicodeUTF8))
        self.passLabel.setText(QtGui.QApplication.translate("LoginWindow", "Password", None, QtGui.QApplication.UnicodeUTF8))
        self.okButton.setText(QtGui.QApplication.translate("LoginWindow", "Ok", None, QtGui.QApplication.UnicodeUTF8))

