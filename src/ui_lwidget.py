# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '../ui/lwidget.ui'
#
# Created: Sun Jun 20 11:11:20 2010
#      by: PyQt4 UI code generator 4.7.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

class Ui_Frame(object):
    def setupUi(self, Frame):
        Frame.setObjectName("Frame")
        Frame.setEnabled(True)
        Frame.resize(712, 529)
        Frame.setFocusPolicy(QtCore.Qt.WheelFocus)
        Frame.setFrameShape(QtGui.QFrame.StyledPanel)
        Frame.setFrameShadow(QtGui.QFrame.Sunken)
        Frame.setStyleSheet("background-color: #333333;border-radius: 10px;")
        self.scrollArea = QtGui.QScrollArea(Frame)
        self.scrollArea.setGeometry(QtCore.QRect(0, 0, 270, 185))
        self.scrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.scrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtGui.QWidget(self.scrollArea)
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 270, 185))
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")

        self.gridLayout = QtGui.QGridLayout(self.scrollAreaWidgetContents)
        self.gridLayout.setHorizontalSpacing(0)
        self.gridLayout.setVerticalSpacing(6)
        self.gridLayout.setObjectName("gridLayout")
        
        self.imageLabel = QtGui.QLabel(self.scrollAreaWidgetContents)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.imageLabel.sizePolicy().hasHeightForWidth())
        self.imageLabel.setSizePolicy(sizePolicy)
        self.imageLabel.setMaximumSize(QtCore.QSize(16777215, 48))
        self.imageLabel.setStyleSheet("background-image: url()")
        self.imageLabel.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.imageLabel.setObjectName("imageLabel")
        self.gridLayout.addWidget(self.imageLabel, 0, 0, 1, 1)

        self.username = QtGui.QLabel(self.scrollAreaWidgetContents)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.username.sizePolicy().hasHeightForWidth())
        self.username.setSizePolicy(sizePolicy)
        self.username.setStyleSheet("background-image: url();font: 8pt \"Lucida Sans Typewriter\";\n"
                                    "color: #BDBDBD;\n"
                                    "background-color:rgb(136,208,239,1%);\n"
                                    "")
        self.username.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.username.setObjectName("username")
        self.username.setWordWrap(True)
        self.gridLayout.addWidget(self.username, 0, 1, 1, 1)


        
        self.tweetText = QtGui.QTextBrowser(self.scrollAreaWidgetContents)
        self.tweetText.setObjectName("tweetText")    
        self.tweetText.setFixedHeight(80);
        self.tweetText.setStyleSheet("QTextBrowser{"
                                     "border:0;"
                                     "background-image: url();\n"
                                     "font: 8pt \"Lucida Sans Typewriter\";\n"
                                     "color: #FFFFFF;\n"
                                     "background-color:#333333;\n"
                                     "}")
#        self.tweetText.setOpenExternalLinks(True)
#        self.tweetText.setOpenLinks(True)
        self.gridLayout.addWidget(self.tweetText, 2, 0, 1, 2)
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.retranslateUi(Frame)
        QtCore.QMetaObject.connectSlotsByName(Frame)

    def retranslateUi(self, Frame):
        Frame.setWindowTitle(QtGui.QApplication.translate("Frame", "Frame", None, QtGui.QApplication.UnicodeUTF8))
        self.imageLabel.setText(QtGui.QApplication.translate("Frame", "Image", None, QtGui.QApplication.UnicodeUTF8))
        self.username.setText(QtGui.QApplication.translate("Frame", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
        #self.tweetLabel.setText(QtGui.QApplication.translate("Frame", "us", None, QtGui.QApplication.UnicodeUTF8))

