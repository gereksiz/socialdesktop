# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '../ui/mainwindow.ui'
#
# Created: Fri Jun 18 15:34:49 2010
#      by: PyQt4 UI code generator 4.7.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtNetwork import *
from funcs import tweets
from funcs import functions
import settings
import operator
from ui_lwidget import Ui_Frame
from ui_rss import Ui_Rss

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        self.getHeaderGUI(MainWindow)
        self.getFooterGUI(MainWindow)
        
    def getHeaderGUI(self,MainWindow):
        self.tweet = tweets()
        self.funcs = functions()
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1000, 665)
        MainWindow.move(150,50)
        MainWindow.setStyleSheet("background-color: #222222;")
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        MainWindow.setSizePolicy(sizePolicy)
        #MainWindow.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        MainWindow.setWindowFlags(QtCore.Qt.WindowMinimizeButtonHint | QtCore.Qt.WindowCloseButtonHint)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        
        menubar = QtGui.QMenuBar(MainWindow)
        menubar.setGeometry(QtCore.QRect(0, 0, 1000, 21))
        menubar.setObjectName("menubar")
        menubar.setStyleSheet( "QMenuBar {"
                               "background-color: #222222;"
                               "border-color:#222222;"
                               "color:#FFFFFF;"
                               "}"
                                "QMenuBar::item {"
                                "spacing: 3px;"
                                "padding: 1px 4px;"
                                "background: #222222;"
                                "border-radius: 4px;"
                                "color:#FFFFFF;"
                                "}"

                                "QMenuBar::item:selected {"
                                "background: #333333;"
                                "color:#FFFFFF;"
                                "}"

                                "QMenuBar::item:pressed {"     
                                "background: #333333;"
                                "color:#FFFFFF;"
                                "}")
        
        menuFile = QtGui.QMenu(menubar)
        menuFile.setStyleSheet("border-color:#222222;color:#FFFFFF;")
        menuFile.setObjectName("menuFile")
        
        MainWindow.setMenuBar(menubar)
        
        exit = QtGui.QAction(QtGui.QIcon('./icons/Closed.png'), 'Exit', MainWindow)
        exit.setShortcut('Ctrl+Q')
        exit.setStatusTip('Exit application')
        MainWindow.connect(exit, QtCore.SIGNAL('triggered()'), QtCore.SLOT('close()'))
        
        menuFile.addSeparator()
        menuFile.addAction(exit)
        menubar.addAction(menuFile.menuAction())
        
        menuFile.setTitle(QtGui.QApplication.translate("MainWindow", "File", None, QtGui.QApplication.UnicodeUTF8))

        self.tabWidget = QtGui.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(0, 66, 1000, 545))
        self.tabWidget.setObjectName("tabWidget")
        self.tabWidget.setMovable(True)
        
        self.settings=settings.settingsXml()
        self.accsInf=self.settings.getAccountsInf()
        for acc in range(len(self.accsInf)):
            self.tab = QtGui.QWidget()
            self.tab.setObjectName("tab")            
            if self.accsInf[acc] =="Twitter":
                self.getTwitLayout(MainWindow)
                self.tabWidget.addTab(self.tab, self.accsInf[acc])
            if self.accsInf[acc] =="FriendFeed":
                self.getFriendFeedLayout(MainWindow)
                self.tabWidget.addTab(self.tab, self.accsInf[acc])
            if self.accsInf[acc] =="Facebook":
                self.getFacebookLayout(MainWindow)
                self.tabWidget.addTab(self.tab, self.accsInf[acc])
            if self.accsInf[acc] =="Rss":
                self.getRssLayout(MainWindow)
                self.tabWidget.addTab(self.tab, self.accsInf[acc])
                
    def getFooterGUI(self,MainWindow):
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.retranslateUi(MainWindow)
        QMetaObject.connectSlotsByName(MainWindow)
           
    def getFacebookLayout(self,MainWindow):
        self.nameLabel = QtGui.QLabel(self.tab)
        self.nameLabel.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.nameLabel.setStyleSheet("color:#0084B4;\n"
"font: 8pt \"Lucida Sans Typewriter\";")
        self.nameLabel.setBaseSize(QtCore.QSize(25, 10))
        self.nameLabel.setAutoFillBackground(False)
        self.nameLabel.setTextFormat(QtCore.Qt.RichText)
        self.nameLabel.setScaledContents(True)
        self.nameLabel.setWordWrap(True)
        self.nameLabel.setObjectName("name")
        self.nameLabel.setText("FAcebook")


    def getFriendFeedLayout(self,MainWindow):
        self.nameLabel = QtGui.QLabel(self.tab)
        self.nameLabel.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.nameLabel.setStyleSheet("color:#0084B4;\n"
"font: 8pt \"Lucida Sans Typewriter\";")
        self.nameLabel.setBaseSize(QtCore.QSize(25, 10))
        self.nameLabel.setAutoFillBackground(False)
        self.nameLabel.setTextFormat(QtCore.Qt.RichText)
        self.nameLabel.setScaledContents(True)
        self.nameLabel.setWordWrap(True)
        self.nameLabel.setObjectName("name")
        self.nameLabel.setText("asdasdasd")
    
    
    
    def getRssLayout(self,MainWindow):
        self.rssTab = QtGui.QTabWidget(self.tab)
        self.rssTab.setGeometry(QtCore.QRect(0, 0, 1000, 515))
        self.rssTab.setTabPosition(QtGui.QTabWidget.West)
        self.rssTab.setObjectName("rssTab")
        
        self.rssInfo = self.settings.getRssInfo()
        self.rssLink = self.settings.getRssLinks()
         
        for rss in range(len(self.rssInfo)):
            self.rssTabWidget = QtGui.QWidget()
            self.rssTabWidget.setObjectName("rssTabWidget")
            
            self.scrollArea = QtGui.QScrollArea(self.rssTabWidget)
            self.scrollArea.setGeometry(QRect(0, 0, 964, 510))
            self.scrollArea.setBaseSize(QSize(0, 0))
            self.scrollArea.setStyleSheet("background-color: #222222;")
            self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
            self.scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
            self.scrollArea.setWidgetResizable(True)
            self.scrollArea.setObjectName("scrollArea")
            self.scrollAreaWidgetContents = QWidget(self.scrollArea)
            self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 964, 510))
            self.scrollAreaWidgetContents.setMouseTracking(True)
            self.scrollAreaWidgetContents.setAcceptDrops(True)
            self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")

            #self.verticalLayoutWidget.setStyleSheet("background-color:#0084B4;")
            self.verticalLayout = QtGui.QVBoxLayout(self.scrollAreaWidgetContents)
            self.verticalLayout.setObjectName("verticalLayout")
            self.scrollArea.setWidget(self.scrollAreaWidgetContents)
            self.getRssGUI(rss)
            self.rssTab.addTab(self.rssTabWidget, self.rssInfo[rss])
        
    def getRssGUI(self,rss):
        rssAre=self.funcs.readRss(self.rssLink[rss])
        
        for rssIs in rssAre.entries:
                frame=QFrame()
                frame.setFixedHeight(150)
                frame.setFixedWidth(925)
                frame.setEnabled(True)
                frame.setMouseTracking(True)
                    
                uirss=Ui_Rss()
                Ui_Rss.setupUi(uirss,frame)
                    
                uirss.rssLink.setText(rssIs.links[0].href) 
                uirss.rssHeader.setText(rssIs.title)
                uirss.rssSummary.setText(rssIs.summary_detail.value)
                self.verticalLayout.addWidget(frame)
        
    def getTwitLayout(self,MainWindow):
        
        self.leftMenu = QtGui.QTabWidget(self.tab)
        self.leftMenu.setGeometry(QtCore.QRect(0, 0, 1000, 515))
        self.leftMenu.setTabPosition(QtGui.QTabWidget.West)
        self.leftMenu.setObjectName("leftMenu")
        self.leftMenuTwitterTab = QtGui.QWidget()
        self.leftMenuTwitterTab.setObjectName("leftMenuTwitterTab")
        
        self.scrollArea = QtGui.QScrollArea(self.leftMenuTwitterTab)
        self.scrollArea.setGeometry(QRect(0, 0, 964, 510))
        self.scrollArea.setBaseSize(QSize(0, 0))
        self.scrollArea.setStyleSheet("background-color: #222222;")
        self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents_2 = QWidget(self.scrollArea)
        self.scrollAreaWidgetContents_2.setGeometry(QRect(0, 0, 964, 510))
        self.scrollAreaWidgetContents_2.setMouseTracking(True)
        self.scrollAreaWidgetContents_2.setAcceptDrops(True)
        self.scrollAreaWidgetContents_2.setObjectName("scrollAreaWidgetContents_2")
        self.verticalLayoutWidget = QtGui.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(0, 0, 964, 61))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        #self.verticalLayoutWidget.setStyleSheet("background-color:#0084B4;")
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.userPicLabel = QtGui.QLabel(self.verticalLayoutWidget)
        self.userPicLabel.setMaximumSize(QtCore.QSize(48, 16777215))
        self.userPicLabel.setStyleSheet("None")
        self.userPicLabel.setFrameShape(QtGui.QFrame.NoFrame)
        self.userPicLabel.setFrameShadow(QtGui.QFrame.Plain)
        self.userPicLabel.setScaledContents(False)
        self.userPicLabel.setObjectName("userPicLabel")
        self.horizontalLayout_2.addWidget(self.userPicLabel)
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.nameLabel = QtGui.QLabel(self.verticalLayoutWidget)
        self.nameLabel.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.nameLabel.setStyleSheet("color:#0084B4;\n"
                                     "font: 10pt \"Lucida Sans Typewriter\";")
        self.nameLabel.setBaseSize(QtCore.QSize(25, 10))
        self.nameLabel.setAutoFillBackground(False)
        self.nameLabel.setTextFormat(QtCore.Qt.RichText)
        self.nameLabel.setScaledContents(True)
        self.nameLabel.setWordWrap(True)
        self.nameLabel.setObjectName("name")
        self.horizontalLayout_4.addWidget(self.nameLabel)
        self.followingLabel = QtGui.QLabel(self.verticalLayoutWidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.followingLabel.sizePolicy().hasHeightForWidth())
        self.followingLabel.setSizePolicy(sizePolicy)
        self.followingLabel.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.followingLabel.setStyleSheet("color:#0084B4;\n"
                                          "font: 10pt \"Lucida Sans Typewriter\";\n")
        self.followingLabel.setScaledContents(True)
        self.followingLabel.setObjectName("followingLabel")
        self.horizontalLayout_4.addWidget(self.followingLabel)
        self.followerLabel = QtGui.QLabel(self.verticalLayoutWidget)
        self.followerLabel.setMaximumSize(QtCore.QSize(250, 16777215))
        self.followerLabel.setStyleSheet("color:#0084B4;\n"
                                         "font: 10pt \"Lucida Sans Typewriter\";")
        self.followerLabel.setScaledContents(True)
        self.followerLabel.setObjectName("followerLabel")
        self.horizontalLayout_4.addWidget(self.followerLabel)
        self.verticalLayout_3.addLayout(self.horizontalLayout_4)
        self.tweetLabel = QtGui.QLabel(self.verticalLayoutWidget)
        self.tweetLabel.setObjectName("tweetLabel")
        self.tweetLabel.setStyleSheet("color:#0084B4;\n"
                                      "font: 8pt \"Lucida Sans Typewriter\";")
        self.tweetLabel.setWordWrap(True)
        self.verticalLayout_3.addWidget(self.tweetLabel)
        self.horizontalLayout_2.addLayout(self.verticalLayout_3)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.gridLayout = QGridLayout(self.scrollAreaWidgetContents_2)
        self.gridLayout.setSizeConstraint(QLayout.SetMinimumSize)
        self.gridLayout.setObjectName("gridLayout")
        self.scrollArea.setWidget(self.scrollAreaWidgetContents_2)
        
        self.GetTwitterGUI()
        
        self.leftMenu.addTab(self.leftMenuTwitterTab, "Tweets")
        self.tab_2 = QtGui.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.pushButton_3 = QtGui.QPushButton(self.tab_2)
        self.pushButton_3.setGeometry(QtCore.QRect(90, 60, 93, 27))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.setText("pushButton_3")
        self.leftMenu.addTab(self.tab_2, "Direct Messages")
        
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "MainWindow", None, QtGui.QApplication.UnicodeUTF8))
        
        self.tweet = tweets()
        self.funcs = functions()
        self.user = self.tweet.getUser()
        self.nameLabel.setText(self.user['name'])
        self.tweetLabel.setText(self.user['status']['text'])
        
#        if not self.funcs.userTwitterImageExits(self.user['profile_image_url'], self.user['screen_name']):
        userimage = self.funcs.getuserTwitterImageFrom(self.user['profile_image_url'], self.user['screen_name'])
#            print "Downloaded"
#        else:
#            userimage = "./images/twitter/" + self.user['screen_name'] + self.user['profile_image_url'][-4:]
    
        self.userPicLabel.setPixmap(QPixmap(userimage))
        self.followerLabel.setText(str(self.user['followers_count']) + " followers")
        self.followingLabel.setText(str(self.user['friends_count']) + " following")
        
    def GetTwitterGUI(self):
        x = 0
        y = 0
        gr = 0
        self.tweetss = self.tweet.getUsersFriendsTweet()
        num = len(self.tweetss)
        kalan = operator.mod(num, 3)

        for gr in range(0, num - kalan, 3):
            if operator.mod(gr, 3) is 0 and gr is not 0:
                x = x + 1
                y = y + 1
            for x in range(3):
                #if not self.funcs.userTwitterImageExits(self.tweetss[gr]['user']['profile_image_url'],self.tweetss[gr]['user']['screen_name']):
                image = self.funcs.getuserTwitterImageFrom(self.tweetss[gr]['user']['profile_image_url'], self.tweetss[gr]['user']['screen_name'])
                #        print "indirdi"
                #else:
                #        image="./images/twitter/"+self.tweetss[gr]['user']['screen_name']+self.tweetss[gr]['user']['profile_image_url'][-4:]
            
                    
                frame=QFrame()
                frame.setFixedHeight(185)
                frame.setFixedWidth(270)
                frame.setEnabled(True)
                frame.setMouseTracking(True)
                    
                uiframe=Ui_Frame()
                Ui_Frame.setupUi(uiframe,frame)
                    
                uiframe.imageLabel.setPixmap(QPixmap(image)) 
                uiframe.imageLabel.setToolTip(self.tweetss[gr]['user']['name'])           
                uiframe.tweetText.setText(QString(self.tweetss[gr]['text']))
                created__at = self.tweetss[gr]['created_at'].split(' ')
                uiframe.username.setText(self.tweetss[gr]['user']['screen_name'] + ", via "+ self.tweetss[gr]['source'] + ", "+created__at[0] +" "+ created__at[2] +" "+ created__at[1] +" "+ created__at[5] +", "+ created__at[3])
                uiframe.username.setEnabled(True)

                self.gridLayout.addWidget(frame, y + 1, x, 1, 1)
                    
                gr = gr + 1

        x = 0
        for far in range(kalan):
            
            if not self.funcs.userTwitterImageExits(self.tweetss[gr]['user']['profile_image_url'],self.tweetss[gr]['user']['screen_name']):
                        image = self.funcs.getuserTwitterImageFrom(self.tweetss[gr]['user']['profile_image_url'], self.tweetss[gr]['user']['screen_name'])
                        print "indirdi"
            else:
                        image="./images/twitter/"+self.tweetss[gr]['user']['screen_name']+self.tweetss[gr]['user']['profile_image_url'][-4:]
            
            frame=QFrame()
            frame.setFixedHeight(185)
            frame.setFixedWidth(270)
            frame.setEnabled(True)
            frame.setMouseTracking(True)
                    
            uiframe=Ui_Frame()
            Ui_Frame.setupUi(uiframe,frame)
                    
            uiframe.imageLabel.setPixmap(QPixmap(image)) 
            uiframe.imageLabel.setToolTip(self.tweetss[gr]['user']['name'])           
            uiframe.tweetText.setText(QString(self.tweetss[gr]['text']))
            created__at = self.tweetss[gr]['created_at'].split(' ')
            uiframe.username.setText(self.tweetss[gr]['user']['screen_name'] + ", via "+ self.tweetss[gr]['source'] + ", "+created__at[0] +" "+ created__at[2] +" "+ created__at[1] +" "+ created__at[5] +", "+ created__at[3])
            #uiframe.username.setEnabled(True)
            self.gridLayout.addWidget(frame, y + 2, x, 1, 1)
            
            x = x + 1
            gr = gr + 1
    def retranslateUi(self, MainWindow):
        '''asdasd'''

