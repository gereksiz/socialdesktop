# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'rss.ui'
#
# Created: Sat Nov 13 14:18:15 2010
#      by: PyQt4 UI code generator 4.7.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

class Ui_Rss(object):
    def setupUi(self, Frame):
        Frame.setObjectName("Frame")
        Frame.resize(925, 150)
        Frame.setFrameShape(QtGui.QFrame.StyledPanel)
        Frame.setFrameShadow(QtGui.QFrame.Raised)
        self.gridLayoutWidget = QtGui.QWidget(Frame)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(0, 0, 925, 150))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtGui.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.rssHeader = QtGui.QLabel(self.gridLayoutWidget)
        self.rssHeader.setObjectName("rssHeader")
        self.rssHeader.setStyleSheet("background-image: url();font: 8pt \"Lucida Sans Typewriter\";\n"
                                    "color: #BDBDBD;\n"
                                    "background-color:rgb(136,208,239,1%);\n")
        self.gridLayout.addWidget(self.rssHeader, 0, 0, 1, 1)
        self.rssSummary = QtGui.QTextEdit(self.gridLayoutWidget)
        self.rssSummary.setObjectName("rssSummary")
        self.rssSummary.setFixedHeight(80);
        self.rssSummary.setStyleSheet("border:0;"
                                     "font: 8pt \"Lucida Sans Typewriter\";\n"
                                     "color: #FFFFFF;\n"
                                     "background-color:#333333;\n")
        
        self.gridLayout.addWidget(self.rssSummary, 1, 0, 1, 2)
        self.rssLink = QtGui.QLabel(self.gridLayoutWidget)
        self.rssLink.setStyleSheet("background-image: url();font: 8pt \"Lucida Sans Typewriter\";\n"
                                    "color: #BDBDBD;\n"
                                    "background-color:rgb(136,208,239,1%);\n")
        self.rssLink.setObjectName("rssLink")
        self.gridLayout.addWidget(self.rssLink, 0, 1, 1, 1)
        QtCore.QMetaObject.connectSlotsByName(Frame)
